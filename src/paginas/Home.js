import React from 'react';
import ContentHeader from '../componentes/ContentHeader';
import Footer from '../componentes/Footer';
import NavBar from '../componentes/Navbar';
import SidebarContainer from '../componentes/SidebarContaier';
import { Link } from 'react-router-dom';

const Home = () => {
    return (
        <div className="wrapper">
            <NavBar></NavBar>
            <SidebarContainer></SidebarContainer>
            <div className="content-wrapper">
                <ContentHeader
                    titulo={"Dashborad"}
                    breadCrumb1={"Inicio"}
                    breadCrumb2={"Dashboard"}
                    ruta1={"/home"}
                />

                <section className="content">
                    <div className="container-fluid">
                        <div className="row">
                            <div className="col-lg-3 col-6">
                                <div className="small-box bg-info">
                                    <div className="inner">
                                        <h3>Mi dulce online</h3>
                                        <p>&nbsp;</p>
                                    </div>
                                    <div className="icon">
                                        <i className="fa fa-edit"></i>
                                    </div>
                                    <Link to={"/proyectos-admin"} className="small-box-footer"> Ver Productos
                                        <i className="fas fa-arrow-circle-right" />
                                    </Link>
                                </div>
                            </div>

                        </div>
                    </div>
                </section>
            </div >
            <Footer></Footer>
        </div >
    );
}

export default Home;